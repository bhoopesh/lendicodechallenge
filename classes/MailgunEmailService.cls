/*@author 		: Bhoopesh Chauhan
 * @description : A service class to facilitate the sending emails via Mailgun Api
 * @date 		: 14th April 2021
 * */
public class MailgunEmailService {
    
    /**
     * @description Method to send email using Mailgun API
     * @param  sendEmailWrapStr instance of the wrapper class MailgunSendEMailWrapper. This wrapper contiains info which is used for sending emails
     */
    @future(callout=true)
    public static void sendEmail(String sendEmailWrapStr){
        
        MailgunSendEMailWrapper sendEmailWrap = (MailgunSendEMailWrapper)Json.deserialize(sendEmailWrapStr,MailgunSendEMailWrapper.class);

        // Prepare multipart form-data for the body of the Http Request
        String boundary = '------------' + String.valueOf(DateTime.now().getTime());
        String body  = '';
        body += HttpRequestHelper.createMultiFormDataElement(boundary, 'from', sendEmailWrap.fromAddr);
        body += HttpRequestHelper.createMultiFormDataElement(boundary, 'to', sendEmailWrap.toAddresses);
        body += HttpRequestHelper.createMultiFormDataElement(boundary, 'subject', sendEmailWrap.Subject);
        body += HttpRequestHelper.createMultiFormDataElement(boundary, 'text', sendEmailWrap.bodyText);
        body += HttpRequestHelper.createMultiFormDataElement(boundary, 'recipient-variables', sendEmailWrap.recipientVariables);
        body += '--' + boundary + '--\r\n';
        
        
        HttpRequest req = new HttpRequest();
        
        //Request endpoint. Used named credential. Authorization header added by name credential to the request
        req.setEndpoint('callout:Mailgun/messages');
        req.setMethod('POST');
        req.setHeader('Content-Type','multipart/form-data; boundary='+ boundary);
        req.setBody(body);
		HttpResponse res;
        
        try{
            //invoke the method to send HttpRequest
            MailgunEmailServiceHelper.sendRequest(req);
        }
        //Api callout timeout. Retry Api call once again
        catch(System.CalloutException ex) { 
            try{
            	MailgunEmailServiceHelper.sendRequest(req);
            }
            catch(System.CalloutException excptn){
            	//Api call failed the second time thrwo exception and publish event
                //The event is subscribed by process builder
                //Process builder then sends a custom notification to the user who made the Account record update, informing about the failure in email delivery
                //Process builder also logs an record in the Error_Log__c object
                MailgunEmailServiceHelper.publishEmailErrorPE((Exception)excptn, req);	    
            }
            
        }
        catch (Exception ex){
            //Api call failed for reason other than CalloutException. Do not retry. Publish email error event
            //The event is subscribed by process builder
            //Process builder then sends a custom notification to the user who made the Account record update, informing about the failure in email delivery
            //Process builder also logs an record in the Error_Log__c object
        	MailgunEmailServiceHelper.publishEmailErrorPE(ex, req);
        }

    }

}