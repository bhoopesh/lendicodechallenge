/**
 * @author      : Bhoopesh Chauhan
 * @date        : 14th April 2021
 * @description : Mock failure implementation for the Mailgun email API. To be used in test classes
 */
@isTest
global class MailgunEmailCallFailureMock implements HttpCalloutMock {
    
    /**
     * @description method to send back the mock response
     * @param  req Http Request instance
     * @return     Http Response
     */
    global HttpResponse respond(HttpRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"failure"}');
        res.setStatusCode(400);
        res.setStatus('Failure');
        return res;
    }
}