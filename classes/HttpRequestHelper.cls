/**
 * @author      : Bhoopesh Chauhan
 * @date        : 14th April 2021
 * @description : Helper class for preparing Http Request
 */
public class HttpRequestHelper {
	
        /**
         * @description Helper method to create multipart form-data to be required for Mailgun email service HTTP Request
         * @param  boundary A randonmly generated boundary string
         * @param  name     Name of the form data
         * @param  val      value for the formdata
         * @return          return the multipart form-data
         */
        public static String createMultiFormDataElement(String boundary, String name, String val){
                String param = '--' + boundary+'\r\n'
                + 'Content-Disposition: form-data; name="' + name + '"'
                + '\r\n\r\n' + val + '\r\n';
                return param;
	}
}