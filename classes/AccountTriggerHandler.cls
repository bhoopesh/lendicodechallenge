/**
 * @author      : Bhoopesh Chauhan
 * @date        : 14th April 2021
 * @description : Class to act as handler for Account trigger actions
 */
public with sharing class AccountTriggerHandler {
	
    /**
     * @description Method to send email using the Mailgun API to client. We send email to Accounts who had their Name updated and their name 
     *              now begins with 'A'/'a' and ends with 'Z'/'z'.
     * @param  newAccountList Trigger.new list
     * @param  oldAccountMap  Trigger.oldMap
     */
    public static  void sendEmailToAcc(List<Account> newAccountList, Map<Id,Account> oldAccountMap){
        
        //Find the list of Accounts who have their name field updatd
        List<Account> updatedAccountList;
        
        //If it is trigger.insert then trigger.new is the list of Accounts we are looking for
        if(trigger.isInsert){
        	updatedAccountList = newAccountList;
        }
        //Else find the record which had their name field updated in this trigger context. This is so that we run our trigger methods selectively
        else if(trigger.isUpdate){
            //call method to fetch accounts which had their 'Name' field updated
            updatedAccountList = AccountTriggerHelper.getChangedValueList(newAccountList,oldAccountMap, 'Name') ;
        }

        //Find the accounts we need to send email To
        //From the Accounts who had their name field updated
        List<Account> accountsToSendEmail;
        
        if(!updatedAccountList.isEmpty() && updatedAccountList != null){
            //Fetch Account records which fulfill the email send criteria
            accountsToSendEmail = AccountTriggerHelper.getAccountsToSendEmail(updatedAccountList);
        }

        //Send the email
        if(accountsToSendEmail!= null && !accountsToSendEmail.isEmpty()){
            
            //Prepare the wrapper which contains Account info to be used in email
            MailgunEmailService mailgunEmailSrvc = new MailgunEmailService();
            MailgunSendEMailWrapper sendEmailWrap = AccountTriggerHelper.getMailgunSendEmailWrap(accountsToSendEmail);

            //Call the emails service to send the emails
            MailgunEmailService.sendEmail(Json.serialize(sendEmailWrap));
        }

    }
    
    
}