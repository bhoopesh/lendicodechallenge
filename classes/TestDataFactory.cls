/*@author 		: Bhoopesh Chauhan
 * @description : A class to prepare data for test classes. As test data creation facilitating class
 * @date 		: 14th April 2021
 * */
@isTest
public class TestDataFactory {
	
    /**
     * @description method to create test Account records
     * @param  num       num of test records to create
     * @param  insertAcc Bollean flag to determine if the method should insert the records or not
     * @return           return requested list<Accounts>
     */
    public static List<Account> createAccounts(Integer num, Boolean insertAcc){
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<num;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i, Email__c = 'test'+i+'@gmail.com'));
        }
        if(insertAcc){
         	insert testAccts;  
        }
        
        return testAccts;
    }
}