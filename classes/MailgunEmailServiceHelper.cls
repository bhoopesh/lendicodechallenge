/*@author 		: Bhoopesh Chauhan
 * @description : A Mailgun email service helper class to assist with the API call
 * @date 		: 14th April 2021
 * */
public class MailgunEmailServiceHelper {
	
    /**
     * @description Method to send HttpRequest. If the status returned is not 200 throw Callout exception so that the exception can be caught and the 
     *              Api call can be tried once more or elegantly handled
     * @param  req  Http Request to send
     */
    public static void sendRequest(HttpRequest req){
    	HttpResponse res = new Http().send(req);
        if(res.getStatusCode()!=200){
            throw new CalloutException('An error occured during email send'+'::'+res.getStatusCode()+'::'+res.getStatus());    
        }
    }
    
    /**
     * @description publishEmailErrorPE description
     * @param  ex  ex description
     * @param  req req description
     */
    public static void publishEmailErrorPE(Exception ex, HttpRequest req){
    	List<Mailgun_Email_Error__e> mailErrorEvents = new List<Mailgun_Email_Error__e>();
        mailErrorEvents.add(new Mailgun_Email_Error__e(Error_Message__c = ex.getMessage(), Payload_Detail__c = req.toString(), Stack_Trace__c = ex.getStackTraceString(), Type__c = ex.getTypeName()));

        // Call method to publish events.
        //The event is subscribed by process builder
        //Process builder then sends a custom notification to the user who made the Account record update, informing about the failure in email delivery
        //Process builder also logs an record in the Error_Log__c object
        List<Database.SaveResult> results = EventBus.publish(mailErrorEvents);	    
    }
}