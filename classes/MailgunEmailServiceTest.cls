/*@author 		: Bhoopesh Chauhan
 * @description : Test class for the MailgunEmailService
 * @date 		: 14th April 2021
 * */
@isTest
public class MailgunEmailServiceTest {
  
    /**
    * @description test setup method to preapre the testdata to be used by the test methods
    */
    @testSetup static void setup() {
        // Create common test accounts
        List<Account> testAccts = TestDataFactory.createAccounts(5, true);
	  }

    /**
    * @description Test method to test successful API call
    */
    @IsTest private static void testSendEmailSuccess() {
        Test.setMock(HttpCalloutMock.class, new MailgunEmailCallMock());
        
        List<Account> accList = [Select Id, Name, Email__c from Account];
        
        Test.startTest();
        
        //test for account update scenario
        accList[0].Name = 'A to Z';
        update accList[0];
        
        //test for account insert scenario matching the email send criteria
        List<Account> accountListToInsert = TestDataFactory.createAccounts(1, false);
        accountListToInsert[0].Name = 'A 2 Z';
        insert accountListToInsert[0];
        
        Test.stopTest();
        
        //Check for error logs. Error logs should nopt have been created
        List<Error_Log__c> errorLogList = [Select Id from Error_Log__c];
        System.assertEquals(0, errorLogList.size());
      }
    

       /**
       * @description Test method to test failed cessful API call
       */
      @IsTest private static void testSendEmailFailure() {
        Test.setMock(HttpCalloutMock.class, new MailgunEmailCallFailureMock());
        
        List<Account> accList = [Select Id, Name, Email__c from Account];
        
        Test.startTest();
        
         //test for account update scenario
        accList[0].Name = 'A to Z';
        update accList[0];
        
        //test for account insert scenario matching the email send criteria
        insert new Account(Name = 'A2Z', Email__c = 'test'+13+'@gmail.com');
        
        Test.stopTest();
        
        //Check for error logs. Error logs should be created
        List<Error_Log__c> errorLogList = [Select Id from Error_Log__c];
        System.assertEquals(2, errorLogList.size());
        
      }

}