/**
 * @author      : Bhoopesh Chauhan
 * @date        : 14th April 2021
 * @description : Helper class for AccountTriggerHandler
 */
public class AccountTriggerHelper {
    
    /**
     * @description Method to find sObject records who have their requested field value changed in the transaction. It is a generic method
     * @param  newAccountList Trigger.New
     * @param  oldAccountMap  Trigger.OldMap
     * @param  fieldName      Name of the field whose value needs to check
     * @return                List<Account> which had their field value updated. i.e. New value is not same as old
     */
    public static List<Account> getChangedValueList(List<Account> newAccountList, Map<Id,Account> oldAccountMap, String fieldName){
        List<Account> updatedAccountList = new List<Account>();
        for(Account acc : newAccountList ){

            Object newValue  = acc?.get(fieldName);
            Object oldValue = oldAccountMap?.get(acc.id)?.get(fieldName);
            
            if(!String.valueOf(newValue).equalsIgnoreCase(String.valueOf(oldValue)) ){
            	updatedAccountList.add(acc);    	
            }    
        }

        return updatedAccountList;
    }
    
    /**
     * @description Method to check for Account Name criteria. Criteria :  Account Name start with A/a and ends with Z /z
     * @param  accList accList to check the criteria against
     * @return         return list of desired Account records
     */
    public static List<Account> getAccountsToSendEmail(List<Account> accList){
        List<Account> accsToSendEmails =  new List<Account>();
        for(Account acc : accList ){
            //check the account record against the criteria
            if(acc.Name?.startsWithIgnoreCase('A') && acc.Name?.endsWithIgnoreCase('Z') ){
            	accsToSendEmails.add(acc);	   
            }
        }
        
        return accsToSendEmails;
    }
    
    /**
     * @description getMailgunSendEmailWrap. A method to get the wrapper instance of the class MailgunSendEMailWrapper. This wrapper is used for sending emails
     * @param  >accountsToSendEmail >accountsToSendEmail list of accounts we need to send email to
     * @return                      an instance of wrapper class MailgunSendEMailWrapper
     */
    public static MailgunSendEMailWrapper getMailgunSendEmailWrap(List<Account >accountsToSendEmail){
        
        //Default params are stored in metadatatype for increase flexibility and customisability
        Mailgun_Email_Param__mdt emailParam = [Select Id, From__c, Subject__c, Body__c FROM Mailgun_Email_Param__mdt WHERE Type__c = 'Default'];
        
        //prepare the insatnce of MailgunSendEMailWrapper and return
        String fromAddr = emailParam.From__c;
        String toAddresses = '';
        String Subject = emailParam.Subject__c;
        String bodyText = emailParam.Body__c;
        String recipientVariables = '';
        
        String template = '"{0}": {1}"first":"{2}", "id":{3}{4},';
        List<Object> parameters = new List<Object>();
            
        for(Account acc : accountsToSendEmail ){
            if(String.isNotBlank(acc.Email__c)){
            	toAddresses +=	acc.Email__c + ',';
                parameters = new List<Object> {acc.Email__c,'{', acc.Name, Integer.valueof((Math.random() * 100)),'}' };
                recipientVariables += String.format(template, parameters);
            }     
        }
        
        recipientVariables = '{'+recipientVariables.removeEnd(',')+'}';

        toAddresses = toAddresses.removeEnd(',');
        
        return new MailgunSendEMailWrapper(fromAddr,toAddresses,Subject,bodyText,recipientVariables);
    }
    
}