/**
 * @author      : Bhoopesh Chauhan
 * @date        : 14th April 2021
 * @description : Mock successful implementation for the Mailgun email API. To be used in test classes
 */
@isTest
global class MailgunEmailCallMock implements HttpCalloutMock {
    
    /**
     * @description method to send back the mock response
     * @param  req Http Request instance
     * @return     Http Response
     */
    global HttpResponse respond(HttpRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"success"}');
        res.setStatusCode(200);
        res.setStatus('Success');
        return res;
    }
}