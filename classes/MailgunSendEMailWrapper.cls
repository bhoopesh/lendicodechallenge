/*@author 		: Bhoopesh Chauhan
 * @description : A wrapper class which is used to prepare data to be sent to MailgunEmailServiceClass. 
 * @date 		: 14th April 2021
 * */
public class MailgunSendEMailWrapper {
	public String fromAddr;
    public String toAddresses;
    public String Subject;
    public String bodyText;
    public String recipientVariables;
    
    /**
     * @description MailgunSendEMailWrapper constructor
     * @param  fromAddr           fromAddr 
     * @param  toAddresses        toAddresses 
     * @param  Subject            Subject 
     * @param  bodyText           bodyText 
     * @param  recipientVariables recipientVariables 
     */
    public MailgunSendEMailWrapper(String fromAddr, String toAddresses, String Subject, String bodyText, String recipientVariables){
    	this.fromAddr = fromAddr;
        this.toAddresses = toAddresses;
        this.subject = subject;
        this.bodyText = bodyText;
        this.recipientVariables = recipientVariables;
    }

}