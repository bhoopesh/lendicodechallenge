trigger AccountTrigger on Account (before insert, before update, after insert, after update) {
    
    if(Trigger.isBefore){
        if(Trigger.isInsert){
            //Before Insert Logic here
        }
        if(Trigger.isUpdate){
            //Before Update Logic here
        }
    }
    
    if(Trigger.isAfter){

        if(Trigger.isInsert){
            System.debug('After Insert');
            AccountTriggerHandler.sendEmailToAcc(Trigger.new, null);
        }
        if(Trigger.isUpdate){
            AccountTriggerHandler.sendEmailToAcc(Trigger.new, Trigger.oldMap);
        }
		
    }

}