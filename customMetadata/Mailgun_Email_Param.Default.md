<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default</label>
    <protected>false</protected>
    <values>
        <field>Body__c</field>
        <value xsi:type="xsd:string">Excited to have you onboard! You have an unique name A-to-Z</value>
    </values>
    <values>
        <field>From__c</field>
        <value xsi:type="xsd:string">Code Challenge User &lt;mailgun@sandbox78b4b51dbdcc4a59b164507d41d6c2fd.mailgun.org&gt;</value>
    </values>
    <values>
        <field>Subject__c</field>
        <value xsi:type="xsd:string">Hello, %recipient.first%!</value>
    </values>
    <values>
        <field>Type__c</field>
        <value xsi:type="xsd:string">Default</value>
    </values>
</CustomMetadata>
